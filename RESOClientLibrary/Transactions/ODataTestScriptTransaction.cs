﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ReferenceLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESOClientLibrary.Transactions
{
    public class EventRequest
    {
        public string url { get; set; }
        public string outputfile { get; set; }
        public string validationid { get; set; }
        public string method { get; set; }
        public string payload { get; set; }
    }

    public class ODataTestScriptTransaction : ODataTransaction
    {

        public Response responseobject { get; set; }

        public ODataTestScriptTransaction(RESOClientSettings clientsettings)
            : base(clientsettings)
        { 

        }

        public bool ExecuteEvent(RESOClient app, EventRequest eventitem, string outputdirectory, ref RESOLogging debuglog)
        {
            try
            {
                if (debuglog != null) debuglog.LogLabel("EventRequest" + eventitem.url);
                responsedata = app.GetData(eventitem.url);
                responseobject = app.responseobject;

                app.LogData(eventitem.outputfile);
                app.LogData(eventitem.outputfile, responsedata);
                StringBuilder sbresponse = new StringBuilder();
                sbresponse.Append("_____________________________REQUEST_____________________________");
                sbresponse.Append("\r\n");
                sbresponse.Append(app.logrequestheader);
                sbresponse.Append("\r\n");
                sbresponse.Append(eventitem.url);
                sbresponse.Append("\r\n");
                sbresponse.Append("_____________________________RESPONSE_____________________________");
                sbresponse.Append("\r\n");
                sbresponse.Append(responseobject.StatusCode);
                sbresponse.Append("\r\n");
                sbresponse.Append(responseobject.ResponseHeaders);
                sbresponse.Append("\r\n");
                sbresponse.Append(responseobject.ResponsePayload);
                sbresponse.Append("\r\n");

                using (System.IO.StreamWriter file =
                                          new System.IO.StreamWriter(outputdirectory + "\\" + eventitem.outputfile, false))
                {

                    file.WriteLine(sbresponse.ToString());
                }

                if (debuglog != null) debuglog.LogData(sbresponse.ToString());
            }
            catch (Exception ex)
            {
                if(debuglog != null)
                {
                    debuglog.LogException("OdataTestScriptTransaction:ExecuteEvent", ex.Message);
                }
                app.LogData("ERROR", ex.Message);
                return false;
            }
            return true;
        }

        public bool ExecuteEventCsv(RESOClient app, EventRequest eventitem, string outputdirectory, ref RESOLogging debuglog)
        {
            try
            {
                int i = 0;
                var results = new List<object>();
                var url = eventitem.url;

                while (true)
                {
                    if (debuglog != null) debuglog.LogLabel("EventRequest" + eventitem.url);
                    responsedata = app.GetData(url);
                    responseobject = app.responseobject;

                    var content = JsonConvert.DeserializeObject<JObject>(responseobject.ResponsePayload);
                    var value = content["value"];
                    var nextLink = content["@odata.nextLink"]?.ToString();

                    if (value == null || value.Count() < 1) { break; }

                    foreach (var line in value)
                    {
                        results.Add(line);
                    }

                    i += value.Count();
                    
                    if (string.IsNullOrEmpty(nextLink)) { break; }

                    url = nextLink;
                }

                var fileName = eventitem.outputfile;
                fileName = fileName.EndsWith(".csv") ? fileName : fileName + ".csv";

                using (var sw = new System.IO.StreamWriter(outputdirectory + "\\" +  fileName))
                {
                    var l = new HashSet<string>();

                    foreach (var item in results)
                    {
                        foreach (var p in ((JObject)item).Properties())
                        {
                            l.Add(p.Name);
                        }
                    }

                    var columnNames = l.ToList().OrderBy(x => x).ToList();

                    foreach (var c in columnNames)
                    {
                        sw.Write(c + ",");
                    }
                    sw.WriteLine();

                    foreach (JObject office in results)
                    {
                        foreach (var c in columnNames)
                        {
                            if (office[c] != null)
                            {
                                sw.Write(office[c].ToString().Replace(',', ';'));
                            }
                            else
                            {
                                sw.Write("NULL");
                            }
                            sw.Write(",");
                        }

                        sw.WriteLine();
                    }

                    sw.Flush();
                    sw.Close();
                } 
            }
            catch (Exception ex)
            {
                if (debuglog != null)
                {
                    debuglog.LogException("OdataTestScriptTransaction:ExecuteEvent", ex.Message);
                }
                app.LogData("ERROR", ex.Message);
                return false;
            }
            return true;
        }
    }
}
